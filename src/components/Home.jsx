import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ProductList from "./ProductList";
import { connect } from "react-redux";

class Home extends Component {
  // increaseQuantity = (id) => {
  //   const cloneCart = [...this.state.cart];

  //   // kiiểm tra sản phẩm đã tồn tại trong giỏ hàng chưa
  //   const foundIndex = cloneCart.findIndex((item) => {
  //     return item.product.id === id;
  //   });

  //   cloneCart[foundIndex].quantity++;

  //   this.setState({
  //     cart: cloneCart,
  //   });
  // };

  // makePayment = () => {
  //   this.setState({ cart: [] }); 
  // };

  // addToCart = (prod) => {
  //   const cloneCart = [...this.state.cart];

  //   // kiiểm tra sản phẩm đã tồn tại trong giỏ hàng chưa
  //   const foundIndex = cloneCart.findIndex((item) => {
  //     return item.product.id === prod.id;
  //   });

  //   //nếu chưa , push mới const cartItem = {product:prod, quantity: 1}
  //   if (foundIndex === -1) {
  //     const cartItem = { product: prod, quantity: 1 };
  //     cloneCart.push(cartItem);
  //   } else {
  //     // nếu có rồi, tăng quantity
  //     cloneCart[foundIndex].quantity++;
  //   }

  //   this.setState({
  //     cart: cloneCart,
  //   });
  // };

  render() {
    return (
      <div>
        <h1 className="text-center">Bài tập giỏ hàng</h1>
        <h4
          data-toggle="modal"
          data-target="#modelId"
          className="text-center text-danger"
        >
          Giỏ Hàng
        </h4>
        <ProductList />

        {this.props.selectedProduct && <Detail />}
        <Cart />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectedProduct: state.product.selectedProduct,
  };
};
export default connect(mapStateToProps)(Home);
